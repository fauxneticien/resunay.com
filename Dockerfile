FROM bitnami/minideb:stretch

RUN apt-get update && apt-get install -y --no-install-recommends \
    ca-certificates \
    texlive-latex-base \
    texlive-bibtex-extra \
    texlive-fonts-recommended \
    texlive-xetex \
    biber \
    fontconfig \
    subversion \
    gnupg \
    curl && \ 
    apt-get autoclean && apt-get --purge --yes autoremove && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Install Nodejs
RUN curl -fsSL https://deb.nodesource.com/setup_lts.x | bash - && \
    apt-get install -y nodejs

# Install node packages
RUN npm install --unsafe-perm=true && \
    npm install --unsafe-perm=true -g netlify-cli

# Grab Crimson Text Alegreya Sans Google fonts
WORKDIR /usr/share/fonts/truetype/
RUN svn checkout https://github.com/google/fonts/trunk/ofl/crimsontext && \
    svn checkout https://github.com/google/fonts/trunk/ofl/alegreyasanssc

WORKDIR /home
