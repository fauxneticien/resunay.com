const fs   = require('fs'),
      _    = require('lodash'),
      ejs  = require('ejs'),
      Cite = require('citation-js'),
      yaml = require('js-yaml'),

    cite_opts = { type: 'html', style: 'citation-apa' },

  template = yaml
    .safeLoad(fs.readFileSync('src/data/San-Nay_CV.yml'))
    .output
    .map(s => `
    <h2>${s.section}</h2>

        ${s.subsections.map(ss => `
            <% if(research_output['${ss.keyword}']) { %>
                <h3>${ss.title}</h3>
                <%- research_output['${ss.keyword}'] %>
            <% } %>
        `).join('\n')}
    `).join('\n')

// For debugging, make sure template is rendered properly
console.log(template)

const citations = fs.readFileSync('src/data/San-Nay_CV.bib')
    .toString()
    // Split BibTeX file by record (i.e. newline starting with '@')
    .split(/\n(?=@)/)
    // Keep only records (not comments, which start with '%')
    // citation-js can't parse BibTeX comments in its BibTeX string input
    .filter(bib_str => /^@/.test(bib_str))

const grouped_apa = _.chain(citations)
    .map(bib_str => ({
        // Extract year and first keyword from each record for grouping/sorting
        year:    bib_str.match(/Year = \{(.+?)\}/).slice(1).toString(),
        keyword: bib_str.match(/Keywords = \{(.+)\}/).slice(1).toString(),
        bib_str: bib_str
    }))
    // Convert flat array of citations to a nested one, grouped by keyword
    .groupBy('keyword')
    // For each citation, within each group, sort in reverse chronological order
    // then convert to APA HTML using citation-js's Cite().get()
    .mapValues(keyword_group => {
        return _.chain(keyword_group)
            .orderBy('year', 'desc')
            .map(bib_obj => new Cite(bib_obj.bib_str).get(cite_opts))
            .value()
            .join('\n')
            .replace(/San, N\./g, '<b>San, N.</b>')
            .replace(/\\emph[tT]o [aA]ppear in the/g, '<span style="font-style:normal">To appear in the</span>')
            .replace(/\(n.d.\)\./g, '')
            .replace(/Retrieved from ([^<]+)/g, 'Available on <a href="$1">$1</a>')
            .replace(/https:\/\/doi\.org\/([^<]+)/g, 'doi: <a href="https://doi.org/$1" target="_blank">$1</a>.')
    })
    // Extract resulting value from the lodash chain function
    .value()

const html = ejs.render(template, { research_output: grouped_apa })

// For debugging, make sure html is rendered properly
console.log(html)

fs.writeFileSync('build/research_output.html', html)
