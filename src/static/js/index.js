// Dynamically create navigation from <h1> elements in document

$("h1.nav-item").each((index, element) => {
    $("#navbarNav > ul").append(`
        <li class="nav-item">
            <a class="nav-link" href="#${element.id}">
                ${element.innerText}
            </a>
        </li>
    `)
})

// Hide nav after clicking a link within the nav
$("#navbarNav").on("click", "a", event => {
    $(".navbar-toggler").click()
})

// If nav bar active, and click is outside nav bar
// then hide the nav bar
$(document).click(event => {
    var clicked_in_nav = $(event.target).is('[class^="navbar"]')

    if (!clicked_in_nav & $("#navbarNav").hasClass('show')) {
        $(".navbar-toggler").click()
    }

})