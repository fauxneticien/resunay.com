# Source code, build scripts and Docker image for resunay.com

## About

This repository contains the source data, scripts, and Docker image for automating the build of [resunay.com](https://resunay.com). The 'master' data sources for my CV are contained in the `./src/data` folder:

2. A BibTeX file with publications/presentations (`./src/data/San-Nay_CV.bib`):

	```
	...
	@conference{san2018acoustic,
		Address = {Paper presented at the 18th Australian Languages Workshop (ALW). Marysville, Victoria.},
		Author = {San, Nay and Carne, M. and Carew, Margaret and Harvey, Mark and Hercus, Luise and Simpson, Jane},
		Date-Added = {2018-05-19 08:29:39 +0000},
		Date-Modified = {2018-06-09 06:54:34 +0000},
		Keywords = {conf-unreffed},
		Month = {3},
		Title = {An acoustic analysis of pre-stopping in {A}rabana},
		Year = {2018}}
	...
	```
1. Everything else that doesn't fit neatly into a BibTeX file goes into the YAML file (`./src/data/San-Nay_CV.yml`):

	```yaml
	...
	education:
	    formal:
	      - award: M.Res., Linguistics
	        year: 2016
	        gpa: 4.0/4.0
	        thesis:
	          title: "A quantitative analysis of vowel variation in Kaytetye"
	          supervisors:
	            - mip
	...
	```

The various scripts transform and render the data into the website (i.e. HTML) and PDF (via XeLaTeX):

- Website (`build/index.html`): ![](img/www.png)
- PDF (`build/cv.pdf`): ![](img/pdf.png)

## Usage

The easiest way to run the build code is to do it in the provided Docker container, `registry.gitlab.com/fauxneticien/resunay.com`.

```bash
# Clone the repo down locally
git clone git@gitlab.com:fauxneticien/resunay.com.git

# Change into repo directory
cd resunay.com

# Run Docker image using config from docker-compose.yml
docker-compose run --rm resunay

# Fetch dependencies for node scripts
npm install

# Run build task
npm run build

# List output inside the ./build/ folder
ls build
```

## Directory structure

```
|── .vscode/                    <- Settings for the Visual Studio Code editor
|── build/                      <- Empty directory for intermediary build files
|── img/                        <- Screenshots contained within this README file
|── src/
│   ├── data/                   <- BibTeX and YAML data for CV
│   ├── scripts/                <- Helper scripts for transforming the data
│   ├── static/                 <- Assets for website (e.g. CSS/Javascript files)
│   ├── templates/              <- Embedded Javascript 'ejs' template files
│   ├── ├── latex/              <- For PDF generation
│   ├── ├── www/                <- For website generation
|── .gitignore                  <- Keeps build/* and node_modules/* untracked
|── .gitlab-ci.yml              <- Pipeline settings for build and deployment via GitLab Continuous Integration (CI)
|── docker-compose.yml          <- Default settings for running the fauxneticien/resunay.com Docker image
|── Dockerfile                  <- Recipe for building the fauxneticien/resunay.com Docker image
|── LICENSE                     <- MIT license text
|── package-lock.json           <- Version-locked package.json
|── package.json                <- Dependency info, and task definitions (e.g. npm run build, the build task)
|── README.md                   <- This README.md file
|── resunay.com.code-workspace  <- Workspace file for VS Code
```

## Links

- Docker installation: [https://docs.docker.com/install/](https://docs.docker.com/install/)
- Embedded Javascript Templates: [http://ejs.co/](http://ejs.co/)
- Citation.js (for BibTeX -> HTML conversion): [https://citation.js.org/](https://citation.js.org/)
